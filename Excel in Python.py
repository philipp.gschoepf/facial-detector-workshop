import pandas as pd

#how to read data from a spreadsheet
pd.read_csv('example_data.csv')

#store the data in a variable so that we can use it later
data = pd.read_csv('example_data.csv')
print(data)

#we can select a column like this:
data['column1']

#and a row can be selected like this:
data.iloc[1]

#Careful! Python starts counting at 0
data.iloc[0]

#We can also select the column and the row at the same time:
data['column1'][0]

#Everything that is possible in Excel can also be done in Python:
sum(data['column1'])

#It's possible to make changes to the data
print(data['column1'])
data['column1'] += 1
print(data['column1'])


#---- merging pandas data frames together ----
#https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html

#creating another copy of the data
data2 = pd.read_csv('example_data.csv')
#re-naming the columns
data2.columns = ['2a', '2b', '2c']

pd.concat([data, data2])
pd.concat([data, data2], axis = 1)

#example of an SQL-style join on the data
pd.merge(
    data,
    data2,
    how="inner",
    on=None,
    left_on='column3',
    right_on='2c'
)



#libraries that are good to get you started fast
#pandas --> 'Excel in Python'
#numpy  --> wide range of efficient mathematical operations
#sklearn    --> Create AI
#matplotlib --> data visualisation




#---- Situation 1 to use Python: when repetition is killed ----

#2 important concepts to prepare you for the more complex code below
#We can also create new data structures inside Python
new_data = pd.Series()

#One possible way to add some additional data to a Pandas series
new_data = new_data.append(pd.Series(0))
print(new_data)


#Loop constructs such as 'for' allow to iterate rapidly through data
for i in data['column1']:
    print(i)

#Inside loops we can also change the data
for i in data['column1']:
    new_data = new_data.append(pd.Series(i * 10))
    #storing in new_data so that we can keep both old and new data

print(new_data)
print(data['column1'])

#range is often useful in looping
for i in range(0,5):
    print(i)



#---- Situation 2 to use Python: when the file is too big to work in Excel
pd.Series(range(0,10000000)).to_csv('very_big_file.csv', header=['column_a'])

pd.read_csv('very_big_file.csv')
temp = pd.read_csv('very_big_file.csv')['column_a']
temp[10]
temp[100000]



#---- Situation 3 to use Python: things that are entirely not possible in Excel

#example: predicting with an AI model
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification

#let's assume y indicates if it's sunny. X shows information, e.g. about the wind direction and ozone.
X, y = make_classification(n_samples=1000, n_features=2, n_informative=2, n_redundant=0, random_state=0, shuffle=False)
clf = RandomForestClassifier(max_depth=2, random_state=0)
clf.fit(X, y)

#given x1 is 0 and x2 is 10, we expect that the weather is going to be sunny
print(clf.predict([[0, 10]]))
#given x1 is 0 and x2 is 10, we expect that the weather is going to be rainy
print(clf.predict([[0, -20]]))


#---- Situation 4 to use Python: giving your work into the hands of customers or other teams

#API construction with Flask (we will organise a separate course on this topic)