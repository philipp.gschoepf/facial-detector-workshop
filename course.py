

#Real-time demo (visualisation.py)



#--- Loading libraries ----
#coding:utf-8
from imutils import face_utils
import pandas as pd
import dlib
import cv2
from PIL import Image



#----- Supporting code to load the facial landmark detection ------
def facepointer(image):
    rects = detector(image, 1)
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region
        shape = predictor(image, rect)
        shape = face_utils.shape_to_np(shape)
    return (pd.DataFrame(shape))

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

video_capture = cv2.VideoCapture(0)
video_capture.set(cv2.CAP_PROP_FPS,60)

#function to save the image we work on as a local jpg file
def imagesaver(imframe):
    im = Image.fromarray(imframe)
    b, g, r = im.split()
    im = Image.merge("RGB", (r, g, b))
    im.save("./your_file.jpeg")



#----- Grab a frame from the camera input and load as numpy array ------
ret, frame = video_capture.read()


#ret indicates if an image was sucessfully retrieved
print(ret)

#we can therefore use it as a 'safety mechanism'
if ret:
    #then do something with the frame
else:
    #do something to avoid a fatal crash


#checking out how the image looks
imagesaver(frame)
#frame contains the image - this is how a color image looks like
print(frame.shape)
#The image is a vectorised object (in the Numpy library format)
print(type(frame))
#Every pixel has a value between 0 and 255 for red, green and blue (RGB)
print(frame)




#----- Manipulating image arrays ------

#Try to make change some pixel values
frame[0:200, 90:100] = 254
imagesaver(frame)

#Try to paint a big cross
frame[95:105, 0:200] = 254
imagesaver(frame)

#Try to change only one color channel
frame[201:400, 0:200,0] = 254
imagesaver(frame)





#Our facepointer function returns a pandas dataframe of 68 key points' coordiantes in a face
df = facepointer(frame)

#the 68 landmark points are in a pandas dataframe object
print(type(df))

#Pandas looks very similar to Excel users
print(df)

#we can access the first column of the dataframe like this
#the first column holds the horizontal coordinates of the 68 points
print(df[0])
#and the first element of the first column in a similar way
print(df[0][0])

#for the second column - these are the vertical coordinates
print(df[1])
print(df[1][0])



#adding a 10 pixel wide line at the first landmark's coordinate
frame[df[1][0], (df[0][0] - 5):(df[0][0] + 5)] = 254
imagesaver(frame)

#marking the first facial landmark point as 10-pixel high line
frame[(df[1][0] - 5):(df[1][0] + 5), df[0][0]] = 254
imagesaver(frame)


#mark the facial landmarks with crosses by looping over all 68 points and painting crosses
for i in range(0, len(df)):
    print(i)
    #same code as for the crosses that we painted below - only the location in the dataframe is now "i"
    frame[(df[1][i] - 5):(df[1][i] + 5), df[0][i]] = 254
    frame[df[1][i], (df[0][i] - 5):(df[0][i] + 5)] = 254

imagesaver(frame)



#---- short summary of the code until now ----
ret, frame = video_capture.read()
df = facepointer(frame)
for i in range(0, len(df)):
    frame[(df[1][i] - 5):(df[1][i] + 5), df[0][i]] = 254
    frame[df[1][i], (df[0][i] - 5):(df[0][i] + 5)] = 254
imagesaver(frame)



#---- simple 'lifeliness check' by using the user's mouth movement ----
#Checking the vertical coordinates of the upper mouth lip
df[1][62]
#Checking the vertical coordinates of the lower mouth lip
df[1][66]

#Calculate how many pixels are between the lips
df[1][62] - df[1][66]

#using absolute value
abs(df[1][37] - df[1][41])



#Other applications:
#-Identity verification: the placement of the landmarks on a user's face is unique, same as a fingerprint
#-Gaze tracking without additional equipment - by detecting where the eyewhite is obstructed by the iris
#-Head rotation and direction detection
#-Simple smile detection - make our users smile!

