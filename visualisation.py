#coding:utf-8
from imutils import face_utils
import pandas as pd
import dlib
import cv2


#----- Supporting code to load the facial landmark detection ------
def facepointer(image):
    rects = detector(image, 1)
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region
        shape = predictor(image, rect)
        shape = face_utils.shape_to_np(shape)
    return (pd.DataFrame(shape))

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

video_capture = cv2.VideoCapture(0)
video_capture.set(cv2.CAP_PROP_FPS,60)



#----- Continuously looping over the camera input ------
while True:
    # Capture frame-by-frame
    ret, frame = video_capture.read()
    if ret:
        try:
            df = facepointer(frame)

            #mark the facial landmarks with green crosses
            for i in range(0, len(df)):
                frame[(df[1][i] - 5):(df[1][i] + 5), df[0][i]] = 254
                frame[df[1][i], (df[0][i] - 5):(df[0][i] + 5)] = 254


            # Display the resulting frame
            frame = cv2.flip( frame, 1 )

            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(frame, "Iris direction:", (10, 100), font, 3, (0, 255, 0), 2, cv2.LINE_AA)

            #Your code goes here



            cv2.imshow('Video', frame)


            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        except:
            frame = cv2.flip(frame, 1)
            cv2.imshow('Video', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    else:
        video_capture.release()
        cv2.destroyAllWindows()
        break
